﻿// Copyright (c) 2021, Andrej Koslov. Dustributed under BSD-3 License"

using System;
using System.IO;
using System.Windows;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Security.Principal;
using Robosync.Properties;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Robosync
{
    /// <summary>
    /// lass contains all management function for configuration and execution tasks.
    /// All functions and variables are static because, a Manager exist only one time!
    /// </summary>
    public class Manager
    {
        // Default path to config file
        private static readonly string ConfigFile = "cfg\\RoboSync.xml";
        private static readonly string ConfigFileBackup = "cfg\\RoboSync.bak.xml";

        // String for creating new empty config
        private static readonly string NewConfig = "<?xml version = \"1.0\" encoding=\"utf-8\" ?>\n<Config>\n</Config>";

        // Bytes in Mbytes. Used to convert Bytes to Mbytes.
        private static readonly long MBytes = 1048576;

        // De/Serialiser for configuration
        private static XmlSerializer serializerConfig = new XmlSerializer(typeof(Config));
        public static Config config;  // Configuration of application
        private static WindowMain wiMain; // Main window.
        private static WindowTask wiTask; // Window for creating/editing synchronisation task.

        // Authentification for network-share.
        private static ConnectToNetworkShare conNetShare;

        // Expected size in bytes that would be trasfered per task.
        private static long sizeAllBytes;

        // Bytes already tranfered per task
        private static long sizeReadyBytes;
        
        // Bytes already transfered percent per task.
        private static float sizeReadyPercent;

        // Action for refresh main grid (table with tasks in main window).
        private static Action actUpdateTableTasks;

        // Pointer to current processed task. Used to update status of task.
        private static SyncTask currentTask;

        /// <summary>
        /// Constructor for object of class Manager. Get config from xml file. Set reference 
        /// to all forms. Set data-source on table in main window. This function called once
        /// if main window loaded.
        /// </summary>
        /// <param name="wiMain_">Handle to main window</param>
        /// <param name="wiTask_">Handle to windows for task-editor</param>
        public Manager (WindowMain wiMain_, WindowTask wiTask_)
        {
            // Set pointer to all forms
            wiMain = wiMain_;
            wiTask = wiTask_;

            // Init action for refresh main grid.
            actUpdateTableTasks = () => wiMain.grTasks.Items.Refresh();

            // If config directory not exists, create it, if failed exit.
            try
            {
                if (!Directory.Exists("cfg"))
                    Directory.CreateDirectory("cfg");
            } catch
            {
                MessageBox.Show("Failed create directory \"cfg\" for configuration file.", "Failed create direcory", MessageBoxButton.OK);
                Environment.Exit(1);
            }

            // If config file not exists create empty.
            if (!File.Exists(ConfigFile))
            {
                Logger.Info("Create empty configuration file " + ConfigFile);
                File.WriteAllText(ConfigFile, NewConfig);
            }

            // Convert XML file to config object.
            // https://stackoverflow.com/questions/1556874/user-xmlns-was-not-expected-deserializing-twitter-xml
            try
            {
                StreamReader reader = new StreamReader(ConfigFile);
                config = (Config)serializerConfig.Deserialize(reader);

                // Put in config decrypted user and pwd. If one of then empty put both as empty string.
                // If decription failed, probably user is not author (is not original user). In this case
                // set user and pwd to null and show warning.
                for (int i = 0; i < config.Task.Count; i++)
                {
                    if (config.Task[i].AuthPwd != null && config.Task[i].AuthUser != null) 
                    {
                        try
                        {
                            config.Task[i].AuthPwdPlain = SecureTools.Decrypt(config.Task[i].AuthPwd, WindowsIdentity.GetCurrent().User.ToString());
                            config.Task[i].AuthUserPlain = SecureTools.Decrypt(config.Task[i].AuthUser, WindowsIdentity.GetCurrent().User.ToString());
                        } catch
                        {
                            config.Task[i].AuthPwdPlain = null;
                            config.Task[i].AuthUserPlain = null;

                            config.Task[i].AuthPwd = null;
                            config.Task[i].AuthUser = null;

                            Logger.Warn("The task \"" + config.Task[i].Name+ "\", ID "+ config.Task[i].ID+ 
                                ". Failed decrypt username and password for network share. You are not author of the task.");

                            MessageBox.Show(
                                "Failed get username and password for network share. You are not author of the task.",
                                "Error task "+ config.Task[i].Name+ ", ID " + config.Task[i].ID, 
                                MessageBoxButton.OK, MessageBoxImage.Warning);
                        }
                    }
                }
                
                reader.Close();
            }
            catch
            {
                MessageBox.Show("Error by reading configuration file \n" + ConfigFile +
                    "\n Remove file and start application again.",
                   "Error configuration", MessageBoxButton.OK, MessageBoxImage.Error);

                Logger.Error("Error by reading configuration file " + ConfigFile + ". Remove file and start application again.");
                Environment.Exit(1);
            }

            Logger.Info("Successfully get config from " + ConfigFile);

            // Add tasks from config to list of tasks in main window
            // https://docs.microsoft.com/de-de/dotnet/api/system.windows.controls.datagrid?view=net-5.0
            wiMain.grTasks.ItemsSource = config.Task;
        }

        /// <summary>
        /// Start selected tasks. The running synchronisation thread will stop automaticaly
        /// if stop-flag Sync.IsStopping = true.
        /// </summary>
        /// <param name="selected">List of selected tasks </SyncTask></param>
        public static void StartTask(List<SyncTask> selected)
        {
            // Return if synchronisation is running
            if (Syncer.IsRunning)
                return;

            // Reset stop flag.
            Syncer.IsStopping = false;

            // Set flag running
            Syncer.IsRunning = true;

            // Always excluded files in user's home directory
            string exFile = @"^NTUSER\.DAT|^ntuser\.dat";

            // Dummy pattern that never match. This need if string "exclude dir" is empty.
            string exDir = "$.^";

            // Times to meassure elapsed time pro task.
            Stopwatch timer = new Stopwatch();

            // Execute synchronisation for each selected task.
            foreach (SyncTask task in selected)
            {
                // Set pointer to current task.
                currentTask = task;

                Logger.Info("");
                Logger.Info("Sart task ID " + task.ID + " name \""+ task.Name+ "\". Loglevel " + task.Loglevel + ".");

                // Set loglevel for this task
                Logger.SetLoglevel(task.Loglevel);

                // Update status of task and refresh table in main window.
                task.Status = "Preparing";
                UpdateTableTasks();
 
                // If "excluded dir" string is not empty convert to string suitable to use in reg. exp,
                // else use default dummy pattern that never match.
                if (task.XDir != "")
                    exDir = ExcludeToRegex(task.XDir);
                               
                // If exclude for files not empty add default exclude, else stay default.
                if (ExcludeToRegex(task.XFile) != "")
                    exFile = exFile + "|"+ ExcludeToRegex(task.XFile);

                Logger.Info("Task ID "+ task.ID+ " pattern for excluded directories \"" + exDir + "\"");
                Logger.Info("Task ID "+ task.ID+ " pattern for excluded files \"" + exFile + "\"");

                // Create reg expressions to detect excluded file or directories. If failed cancel
                // execution.
                Regex reFile, reDir;
                try
                {
                    reFile = new Regex(exFile);
                } catch (System.Exception e)
                {
                    Logger.Error("Task ID "+ task.ID+ " failed use pattern \"Excluded files\" " + e.Message);
                    MessageBox.Show("Task ID "+ task.ID+ " failed use pattern \"Excluded files\".", "Invalid exclude pattern", MessageBoxButton.OK);
                    Syncer.IsRunning = false;

                    task.Status = "Aborted";
                    UpdateTableTasks();

                    SetLabelOnStartButton("Start");
                    Logger.SetDefaultLogLevel();
                    return;
                }

                try
                {
                    reDir = new Regex(exDir);
                }
                catch (System.Exception e)
                {
                    Logger.Error("Task ID "+ task.ID+ " failed use pattern for \"Excluded directories\" " + e.Message);
                    MessageBox.Show("Task ID "+ task.ID + " failed use pattern \"Excluded directories\".", "Invalid exclude pattern", MessageBoxButton.OK);
                    Syncer.IsRunning = false;
                    
                    task.Status = "Aborted";
                    UpdateTableTasks();

                    SetLabelOnStartButton("Start");
                    Logger.SetDefaultLogLevel();
                    return;
                }

                // Always create object for connection to share although it is not always need.
                // It is stupid but a can't solve some programmatically C# problems. It is not
                // possible: if(conNetShare != null) conNetShare.Disconnect();
                // As path use \\Servername\Sharename Never use full path in task.Destination. 
                // If target folder not exists, authentification will failed!!
                String pathToSharedFolder = extractPathToShare(task.Destination);
                conNetShare = new ConnectToNetworkShare(pathToSharedFolder);

                // If destination is network-share and username and password is not empty, 
                // execute authentification. If failed, cancel the task.
                if (isNetworkShare(task.Destination) & task.AuthUserPlain != null && task.AuthPwdPlain != null)
                {
                    try
                    {
                        conNetShare.Connect(task.AuthUserPlain, task.AuthPwdPlain);
                        Logger.Info("Task ID " + task.ID + " successfully authentificated by network-share");
                    }
                    catch
                    {
                        Logger.Error("Task ID " + task.ID + " failed authentification or network-share is unreachable.");
                        task.Status = "Error";
                        UpdateTableTasks();
                        continue;
                    }
                }

                // If destination is not assessible log error and go to next task.
                if (!Directory.Exists(task.Destination))
                {
                    Logger.Warn("Destination folder " + task.Destination+ " not found. Try create.");
                    Syncer.SafeCreateDir(task.Destination);

                    if (!Directory.Exists(task.Destination))
                    {
                        Logger.Error("Failed create destination folder " + task.Destination);
                        conNetShare.Dispose();
                        task.Status = "Error";
                        UpdateTableTasks();
                        continue;
                    }
                }

                long runtime; // Contains time in milliseconds by some operations
                int errorc = 0; // Amount of error by synchronisation

                try
                {
                    // Set values at begin
                    sizeReadyBytes = 0;
                    sizeReadyPercent = 0;

                    // Differently process task type Backup or Synchronisation
                    //if (task.CreateStructur)
                    //{
                    // Execute task type Backup

                    Syncer.isBackupMode = task.CreateStructur;
                    Syncer.syncSource = task.Source[0].Path;

                        // Open catalog or create new if not exist
                    Catalog catalog = new Catalog(currentTask.Destination);
                        Logger.Info("Task ID " + task.ID + " open catalog " + catalog.pathToCatalogZip);

                        // Prepare all sources in the task and caclulate overall size of tranfered data.
                        timer.Start();
                        decimal syncSizeMB = 0;
                        sizeAllBytes = Syncer.BackupPrepare(SourcesAsArray(task.Source), reDir, reFile, catalog, currentTask.Destination);
                    
                        if (sizeAllBytes > 0)
                            syncSizeMB = sizeAllBytes / MBytes;

                        timer.Stop();
                        timer.Reset();
                        runtime = timer.ElapsedMilliseconds / 1000;
                        decimal msforone = Convert.ToDecimal(timer.ElapsedMilliseconds) / catalog.Count();
                        
                        Logger.Info("Task ID " + task.ID + " amount data to transfer " + syncSizeMB.ToString("n2") +
                            "MB. Preparation took " + runtime.ToString() + " sec. Catalog contains " + 
                                catalog.Count() + " items. Time per item " + msforone.ToString("n2") + "ms.");

                        task.Status = "Ready 0.00%";
                        UpdateTableTasks();

                        // Start syncronisaton in the task
                        timer.Start();

                        errorc = Syncer.BackupStart(task.Destination, reDir, reFile, catalog);
                        
                        // Save catalog
                        catalog.SaveToZip(SourcesAsArray(task.Source));

                    /*} 
                    else 
                    {
                        // Execute task type Synchronisation

                        // Caclulate overall size of tranfered data.
                        timer.Start();
                        decimal syncSizeMB = 0;
                        sizeAllBytes = Syncer.SyncGetSize(task.Source[0].Path, task.Destination, reDir, reFile);
                        
                        if (sizeAllBytes > 0)
                            syncSizeMB = sizeAllBytes / MBytes;

                        timer.Stop();
                        runtime = timer.ElapsedMilliseconds / 1000;
                        timer.Reset();

                        Logger.Info("Task ID " + task.ID + " amount data to transfer " + syncSizeMB.ToString("n2") +
                            "MB. Preparation took " + runtime.ToString() + " sec.");

                        task.Status = "Ready 0.00%";
                        UpdateTableTasks();

                        // Start syncronisaton in the task
                        timer.Start();
                        errorc = Syncer.SyncStart(task.Source[0].Path, task.Destination, reDir, reFile);
                    }*/
                }
                catch (InvalidOperationException e)
                {
                    Logger.Warn("Task ID "+ task.ID+ ". "+ e.Message);
                    task.Status = "Aborted manual.";
                    break; // Exit foreach loop
                }
                catch (System.Exception e)
                {
                    Logger.Error("Task ID "+ task.ID+ " is aborted due unknown error. " + e.Message);
                    task.Status = "Aborted. Unknow error.";
                    break; // Exit foreach loop
                }
                finally
                {
                    timer.Stop();
                    timer.Reset();
                    Syncer.IsRunning = false;
                    UpdateTableTasks();
                    conNetShare.Dispose(); // Logout from network share. If not connected no think happen.
                    currentTask = null;
                }
                
                runtime = timer.ElapsedMilliseconds / 1000;

                // Switch to default and set variable catalog to null to save memory
                SetLabelOnStartButton("Start");
                Logger.SetDefaultLogLevel();
                currentTask = null;

                if (errorc > 0)
                {
                    task.Status = "End with "+ errorc+ " errors";
                    Logger.Error("Task ID "+ task.ID+ " ended with "+ errorc+ " errors. Execution took "+ runtime.ToString()+ " sec.");
                }
                else
                {
                    task.Status = "End successfully";
                    Logger.Info("Task ID "+ task.ID+ " ended successfully. Execution took "+ runtime.ToString()+ " sec.");
                }

                UpdateTableTasks();
            }

            Syncer.IsStopping = false;
            Syncer.IsRunning = false;
            
            SetLabelOnStartButton("Start");
        }



        /// <summary>
        /// Convert list of sources in task to array of strings
        /// </summary>
        /// <param name="task">SyncTask</param>
        /// <returns></returns>
        private static String[] SourcesAsArray(List<SourceItem> sources)
        {
            String[] result = new String[sources.Count];
            for (int i = 0; i < sources.Count; i++)
            {
                result[i] = sources[i].Path;
            }

            return result;
        }

        // Update status of synchronisation in table task. Show ready synchronysed percents.
        // This function called from Syncer after copy each file.
        public static void UpdateStatus(long bytes)
        {
            if (bytes < 1) return;

            sizeReadyBytes += bytes;
            sizeReadyPercent = ((float)sizeReadyBytes / sizeAllBytes * 100);
            currentTask.Status = "Ready " + sizeReadyPercent.ToString("n2")+ "%";
            UpdateTableTasks();
        }

        /// <summary>
        /// Set new label on start button in main form using Dispatcher. Only over Dispatcher is 
        /// possbilble access to windows running in another trhread.
        /// </summary>
        /// <param name="newLabel"> String newLabel - new label.</param>
        private static void SetLabelOnStartButton(String newLabel)
        {
            if (newLabel == null) return;
            if (newLabel == "") return;

            Action action = () => wiMain.buStart.Content = newLabel;
            Application.Current.Dispatcher.Invoke(action);
        }

        /// <summary>
        /// Refresh grid in main windows using dispatcher. You can change data in datasources
        /// of grid but for updaten UI need call this function.
        /// </summary>
        private static void UpdateTableTasks()
        {   
            Application.Current.Dispatcher.Invoke(actUpdateTableTasks);
        }

        /// <summary>
        /// Change task to backup mode 
        /// </summary>
        public static void setBackupMode()
        {
            // Prevent exeption in initial phase
            if (wiTask == null) return;

            // Elemente for sync mode disable and recudce opacity
            wiTask.rbModeSync.IsChecked = false;
            wiTask.txSource.IsEnabled = false;
            wiTask.btSourceSyncBrowse.IsEnabled = false;
            wiTask.blockSync.Opacity = 0.3; 

            // Elemente for backup mode enable and fullopcity
            wiTask.rbModeBackup.IsChecked = true;
            wiTask.liSource.IsEnabled = true;
            wiTask.btSourceBackupBrowse.IsEnabled = true;
            wiTask.btSourceBackupRemove.IsEnabled = true;
            wiTask.blockBackup.Opacity = 1;
        }

        /// <summary>
        /// Change task to synchronisation mode
        /// </summary>
        public static void setSyncMode()
        {
            // Prevent exeption in initial phase
            if (wiTask == null) return;

            // Elemente for sync mode enable and full opacity
            wiTask.rbModeSync.IsChecked = true;
            wiTask.txSource.IsEnabled = true;
            wiTask.btSourceSyncBrowse.IsEnabled = true;
            wiTask.blockSync.Opacity = 1;

            // Elemente for backup mode disable and recduce opacity
            wiTask.rbModeBackup.IsChecked = false;
            wiTask.liSource.IsEnabled = false;
            wiTask.btSourceBackupBrowse.IsEnabled = false;
            wiTask.btSourceBackupRemove.IsEnabled = false;
            wiTask.blockBackup.Opacity = 0.3;
        }

        /// <summary>
        /// Open window-task to create new task. Before open window clear all data.
        /// </summary>
        public static void NewTask()
        {
            wiTask.Title = "Create new task";
            wiTask.txID.Text = "";
            wiTask.txName.Text = "";
            wiTask.txDest.Text = "";
            wiTask.txXDir.Text = "";
            wiTask.txXFile.Text = "";
            wiTask.cbLoglevel.SelectedIndex = 2;
            wiTask.btSave.IsEnabled = false;
            wiTask.liSource.Items.Clear();
            wiTask.txUsername.Text = "";
            wiTask.txPwd.Text = "";

            // Defaul mode backup. Adjust visual elements to backup mode.
            setBackupMode();

            wiTask.btSave.IsEnabled = true;
            wiTask.btCopy.IsEnabled = false;

            wiTask.Show();
        }
          
        /// <summary>
        /// Open window-task to change selected task.
        /// </summary>
        /// <param name="index">Integer. Index of selected task.</param>
        public static void ChangeTask(int index)
        {
            // Set pointer to current selected task
            SyncTask task = config.Task[index];

            wiTask.Title = "Change Task";
            wiTask.txID.Text = task.ID.ToString();
            wiTask.txName.Text = task.Name;
            wiTask.txDest.Text = task.Destination;
            wiTask.txUsername.Text = task.AuthUserPlain;
            wiTask.txPwd.Text = task.AuthPwdPlain;
            wiTask.txXDir.Text = task.XDir;
            wiTask.txXFile.Text = task.XFile;

            // Set log-level index. Index tak from dictionary. If not found set default "INFO"
            if (Logger.loglevelIndex.ContainsKey(task.Loglevel))
            {
                wiTask.cbLoglevel.SelectedIndex = Logger.loglevelIndex[task.Loglevel];
            } else
            {
                wiTask.cbLoglevel.SelectedIndex = Logger.loglevelIndex["INFO"];
            }

            // Fist clean list of sources
            wiTask.liSource.Items.Clear();
            wiTask.txSource.Text = "";
            
            // Set data in task dependet on execution mode (backup or sync).
            if (task.CreateStructur)
            {
                // In backup-mode fill list of sources
                for (var i = 0; i < task.Source.Count; i++)
                {
                    wiTask.liSource.Items.Add(task.Source[i].Path);
                }

                // Adjust visual elements for backup-mode.
                setBackupMode();

            } else
            {
                // In sync-mode fill only fild txSource.
                if (task.Source.Count > 0)
                {
                    wiTask.txSource.Text = task.Source[0].Path;
                }

                // Prepare visual elements for synchronisation-mode.
                setSyncMode();
            }

            wiTask.btSave.IsEnabled = true;
            wiTask.btCopy.IsEnabled = true;

            wiTask.ShowDialog();
        }

        /// <summary>
        /// Delete selected task and save config.
        /// </summary>
        /// <param name="index">Integer. Indext of selected task.</param>
        public static void DeleteTask(int index)
        {
            int ID = config.Task[index].ID;

            MessageBoxResult result = MessageBox.Show("Detele task ID " + ID, "Delete task", MessageBoxButton.OKCancel);

            if (((int)result) == 1) {
                config.Task.RemoveAt(index);
                Logger.Info("Deleted task ID " + ID);
                SaveConfig();
            }
        }

        /// <summary>
        /// Save task and close window-task. If ID not exists add task to config.  
        /// </summary>
        public static void SaveTask()
        {
            // Check if properties of task are valid, if not return.
            if (!isTaskValid()) return; 

            // In backup mode create list of sources from list, else take single path from text-element.
            List<SourceItem> liSource = new List<SourceItem>();
            if (wiTask.rbModeBackup.IsChecked.Value) {

                for (int i = 0; i < wiTask.liSource.Items.Count; i++)
                {
                    try { 
                        SourceItem su = new SourceItem(wiTask.liSource.Items[i].ToString());
                        liSource.Add(su);
                    } catch {
                    }
                }
            } else {

                SourceItem su = new SourceItem(wiTask.txSource.Text);
                liSource.Add(su);
            }

            // If task-id is empty create new task, else by save updat existing or copy.
            if (wiTask.txID.Text == "")
            {
                // Get id for new task
                int newid = GetNextTaskID();

                // Creat new task and add to current selected plan
                SyncTask task = new SyncTask(newid, wiTask.txName.Text, liSource,
                    wiTask.txDest.Text, wiTask.rbModeBackup.IsChecked.Value, wiTask.txXDir.Text,
                        wiTask.txXFile.Text, wiTask.cbLoglevel.Text);

                config.Task.Add(task);
                Logger.Info("Created new task ID " + newid);

            } else
            {   // Case save changes in task

                // Set pointer to current selected task
                SyncTask task = config.Task[wiMain.grTasks.SelectedIndex];

                task.Name = wiTask.txName.Text;
                task.Source = liSource;
                task.Destination = wiTask.txDest.Text;
                
                // Save username and password encrypted. The encryption is not very secure but better as plain text
                task.AuthUser = SecureTools.Encrypt(wiTask.txUsername.Text, WindowsIdentity.GetCurrent().User.ToString());
                task.AuthPwd =  SecureTools.Encrypt(wiTask.txPwd.Text, WindowsIdentity.GetCurrent().User.ToString());

                task.AuthUserPlain = wiTask.txUsername.Text;
                task.AuthPwdPlain = wiTask.txPwd.Text;

                task.CreateStructur = wiTask.rbModeBackup.IsChecked.Value;
                task.XDir = wiTask.txXDir.Text;
                task.XFile = wiTask.txXFile.Text;
                task.Status = "Waiting";
                task.Loglevel = wiTask.cbLoglevel.Text;
                Logger.Info("Save task ID " + task.ID);
            }

            SaveConfig();
            wiMain.grTasks.Items.Refresh();
            wiTask.Hide();
        }

        /// <summary>
        /// Dupplicate current task per set neue ID and add word "Copy" in the name of task
        /// task and try to save.
        /// </summary>
        public static void CopyTask () 
        {
            // Check if properties of task are valid, if not return.
            if (!isTaskValid()) return;

            // Get id for new task
            int newid = GetNextTaskID();

            // In backup mode create list of sources from list, else take single path from text-element.
            List<SourceItem> liSource = new List<SourceItem>();
            if (wiTask.rbModeBackup.IsChecked.Value)
            {

                for (int i = 0; i < wiTask.liSource.Items.Count; i++)
                {
                    SourceItem su = new SourceItem(wiTask.liSource.Items[i].ToString());
                    liSource.Add(su);
                }
            }
            else
            {

                SourceItem su = new SourceItem(wiTask.txSource.Text);
                liSource.Add(su);
            }

            SyncTask task = new SyncTask(newid, wiTask.txName.Text+ " Copy!", liSource,
                    wiTask.txDest.Text, wiTask.rbModeBackup.IsChecked.Value, wiTask.txXDir.Text,
                        wiTask.txXFile.Text, wiTask.cbLoglevel.Text);

            config.Task.Add(task);

            SaveTask();
            wiMain.grTasks.Items.Refresh();
            wiTask.Hide();
        }
        
        /// <summary>
        /// Save config as xml. Befor save create backup and move current config as backup.
        /// !!! If new config is smaler as old, then in file stay part of old config. Because
        /// of it config always write in new file.
        /// </summary>
        private static void SaveConfig()
        {
            try
            {
                if (File.Exists(ConfigFileBackup))
                    File.Delete(ConfigFileBackup);
            }
            catch (Exception e)
            {
                Logger.Error("Failed delete old configuration backup file " + ConfigFileBackup);
                Logger.Error(e.ToString());
                MessageBox.Show("Failed delete old configuration\nbackup file " + ConfigFileBackup,
                    "Failed delete backup configuration", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            try
            {
                File.Move(ConfigFile, ConfigFileBackup);
            }
            catch (Exception e)
            {
                Logger.Error("Failed move configuration file " + ConfigFile + " to backup " + ConfigFileBackup);
                Logger.Error(e.ToString());

                MessageBox.Show("Failed move configuration file " + ConfigFile + "\n to backup " + ConfigFileBackup,
                      "Failed backup configuration", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            try
            {
                FileStream file = File.OpenWrite(ConfigFile);
                serializerConfig.Serialize(file, config);
                file.Close();
            }
            catch (Exception e)
            {
                Logger.Error("Failed save configuration to file" + ConfigFile);
                Logger.Error(e.ToString());
                MessageBox.Show("Failed save configuration to file\n" + ConfigFile,
                    "Failed save configuration", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// Return next free task ID, as last task ID plus 1. By firs task new ID 0.
        /// </summary>
        /// <returns>Integer</returns>
        private static int GetNextTaskID()
        { 
            if(config.Task.Count == 0) return 0;
            
            int newID = config.Task[config.Task.Count - 1].ID + 1;
            Logger.Info("New task ID" + newID);
            return newID;
        }

        /// <summary>
        /// Extract from full path to target only\\servername\sharename. This function called in case
        /// if backup to local device. I know it is stupid :(. If servername or sharnam e not found
        /// return null
        /// </summary>
        /// <param name="pathFull"></param>
        /// <returns>String \\servername\sharename </returns>
        private static String extractPathToShare(String pathFull)
        {
            String[] pathParts = pathFull.Split(new string[] { @"\" }, StringSplitOptions.None);
            if (pathParts.Length > 3)
            {
                return @"\\" + pathParts[2] + @"\" + pathParts[3];
            }

            return null;
        }

        /// <summary>
        /// Check if path is usual path to network share like \\ServerName\ShareName or lokal path
        /// </summary>
        /// <param name="path">String. Path to check</param>
        /// <returns></returns>
        private static bool isNetworkShare(String path)
        {
            // Reg expr for local path
            Regex reLocal = new Regex(@"^[A-Za-z]:\\");

            // If local path return false (invert result)
            return !reLocal.IsMatch(path);
        }

        /// <summary>
        /// Check if properties of task are valid. This function called befor save or copy task.
        /// </summary>
        /// <returns>Boolean.True if all ok, else false.</returns>
        private static bool isTaskValid()
        {
            // Check name of task
            if (wiTask.txName.Text.Length < 5 || wiTask.txName.Text.Length > 50)
            {
                MessageBox.Show("Name of task should be between 5 and 50 characters.",
                    "Invalid name of task", MessageBoxButton.OK);
                return false;
            }

            // Always need destination
            if (wiTask.txDest.Text == "")
            {
                MessageBox.Show("Empty destination",
                    "Invalid destination", MessageBoxButton.OK);
                return false;
            }

            // In backup mode need at least one item in list of sources
            if (wiTask.rbModeBackup.IsChecked.Value && wiTask.liSource.Items.Count == 0)
            {
                MessageBox.Show("For backup need at least one source.",
                    "Missing source", MessageBoxButton.OK);
                return false;
            }

            // In sync mode need source. 
            if (wiTask.rbModeSync.IsChecked.Value && wiTask.txSource.Text == "")
            {
                MessageBox.Show("For synchronisation need source.",
                    "Missing source", MessageBoxButton.OK);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Convert task's parameter exclude-directories or -files to string suitable to use in
        /// regular expression. Elements in exclude sting are separated by "|".
        /// </summary>
        /// <param name="exclude">String-exclude - string as defined in parameter exlude dir or exclude file.</param>
        /// <returns>String suitablle for regexpression</returns>
        public static string ExcludeToRegex(string exclude)
        {
            string result = "";
            
            // If exclude string is empty return empty string.
            if (exclude.Length == 0)
                return result;

            // Create list of excluded elements
            List<String> lsExcludes = new List<String>(exclude.Split('|'));
            string newitem;

            foreach (string item in lsExcludes)
            {
                // Skeep if item is empty
                if (item == "")
                    continue;

                newitem = item.Trim();

                // At begin add "^" if first not "*"
                if(!item.StartsWith("*"))
                    newitem = "^" + item;
                
                // At end add "^" if last not "*"
                if (!item.EndsWith("*"))
                    newitem += "$";
                
                // All "." repace  by "\."
                newitem = newitem.Replace(".", @"\.");

                // All "*" replace by ".+"
                newitem = newitem.Replace("*", @".+");

                // If result not empty add pipe "|" 
                if (result.Length != 0)
                    result += @"|";

                result += newitem;
            }

            // Always remove last pipe
            if (result.LastIndexOf("|") == (result.Length - 1)) {
                result = result.Remove(result.Length - 1);
            }

            return result;
        }

    }
}
