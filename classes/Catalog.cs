﻿
using System;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Robosync
{
    /// <summary>
    /// Type item on file system. May be late I add link, etc ..
    /// </summary>
    public enum fsType { File, Directory, Unknow };

    /// <summary>
    /// Type of operation. 
    /// No - no think to do. Item is unganged.
    /// Add, Update - copy item from source to backup. Item is new or changed.
    /// Delete - delete item in backup. Item not more exist in source.
    /// Deleted - don't process item because parent folder will be deleted. Don't save in new catalog.
    /// </summary>
    public enum operationType {No, Add, Update, Delete, Deleted};

    /// <summary>
    /// The structur reprecent one record in catalog. Based on System.IO.FileSystemInfo
    /// </summary>
    public class CatalogItem
    {
        public String fullname; // Original full path. Take from FileInfo.FullName
        public fsType fstype; // Type item on file-system
        public String tscreate; // Date and time of creation. Used to detect a change.
        public String tschange; // Date and time of last write. Used to detect a change.
        public long length; // The size of the current file in bytes. Used to detect a change.
        public operationType optype; // Type of operation on item. Defaul No.  The properties not saved in CSV.

        /// <summary>
        /// Create catalog item from FileInfo object.
        /// </summary>
        /// <param name="fi">FileInfo object</param>
        public CatalogItem(FileInfo fi, operationType operation)
        {
            fullname = fi.FullName;
            fstype = fsType.File;
            tscreate = fi.CreationTime.ToString("G");
            tschange = fi.LastWriteTime.ToString("G");
            length = fi.Length;
            optype = operation;
        }

        /// <summary>
        /// Create catalog item from DirectroyInfo object.
        /// </summary>
        /// <param name="di">DirectoryInfo object</param>
        public CatalogItem(DirectoryInfo di, operationType operation)
        {
            fullname = di.FullName;
            fstype = fsType.Directory;
            tscreate = di.CreationTime.ToString("G");
            tschange = di.LastWriteTime.ToString("G");
            length = 0;
            optype = operation;
        }

        /// <summary>
        /// Create catalog item from data in csv file. All parameters are string.
        /// </summary>
        /// <param name="ofpath">full path in source</param>
        /// <param name="type">Type. D for directory, F for file. U Unknown</param>
        /// <param name="create">Date and time of creation</param>
        /// <param name="change">Date and time of last write</param>
        /// <param name="length">Length of file. Applicable only for type File. All other types 0.</param>
        public CatalogItem(String ofpath, String type, String create, String change, String length)
        {
            fullname = ofpath;
            
            this.length = 0; // stay if type not File

            if (type == fsType.File.ToString())
            {
                this.fstype = fsType.File;
                this.length = long.Parse(length);
            }
            else if (type == fsType.Directory.ToString())
            {
                this.fstype = fsType.Directory;
                this.length = 0;
            }  else
            {
                this.fstype = fsType.Unknow;
                this.length = 0;
            }

            this.tscreate = create;
            this.tschange = change;
            this.optype = operationType.No;
        }

        /// <summary>
        /// Update properties of item without change the type. Used only for file.
        /// </summary>
        /// <param name="fi">FileInfo object</param>
        public void Update(FileInfo fi)
        {
            length = fi.Length;
            tscreate = fi.CreationTime.ToString("G");
            tschange = fi.LastWriteTime.ToString("G");
            optype = operationType.Update;
        }

        /// <summary>
        /// If copy or delete failed, change propries of item to repeat operation next time.
        /// If failed copy existing item (update) or delete -> set operation No and size 0, etc...
        /// If failed copy new item (add) -> set operation Delete. Items with operation Delete will 
        /// be not saved in catalog.
        /// </summary>
        public void SetOperationFailed()
        {
            length = 0;
            tscreate = "0";
            tschange = "0";

            if(optype == operationType.Add)
            {
                optype = operationType.Delete;
            } else
            {
                length = 0;
                tscreate = "0";
                tschange = "0";
                optype = operationType.No;
            }
        }

        /// <summary>
        /// Update properties of item without change the type. Used only for file.
        /// </summary>
        /// <param name="di">DirectroyInfo object</param>
        public void Update(DirectoryInfo di)
        {
            tscreate = di.CreationTime.ToString("G");
            tschange = di.LastWriteTime.ToString("G");
        }

        /// <summary>
        /// Output item as csv-string.
        /// </summary>
        /// <returns>String</returns>
        public override string ToString()
        {
            return fullname + Catalog.CSV_SPR + fstype.ToString() + Catalog.CSV_SPR +
                tscreate + Catalog.CSV_SPR + tschange + Catalog.CSV_SPR + length.ToString() + Catalog.CSV_SPR;
        }

    // End class CatalogItem
    }

    /// <summary>
    /// The catalog is like database all backed up files. Saved as zipped CSV in root of backup
    /// target. As targed may be path to local device or network-share.
    /// </summary>
    public class Catalog
    {
        internal static readonly Char CSV_SPR = '|'; // Separator for CSV catalog file.
        internal static readonly String FS_SPR = @"\"; // Separator for path on file-system. On Windows alwasys the same.
        internal static readonly String META = "META"; // Identifier for meta record
        internal static readonly String SOURCE = "SOURCE"; // Identifiert for source records
        internal static readonly String NEW_LINE = Environment.NewLine;
        internal static readonly String CATALOG_FILE = @"catalog.csv"; // Name of file for catalog in ZIP
        internal static readonly String CATALOG_ZIP = @"catalog.zip"; // Name of ZIP-File for catalog.

        // Path to ZIP-File containung catalog.zip Set in constructor of catalog.
        public String pathToCatalogZip { get; }

        // List of sources (source folders) that taken from CSV. Need to detect if source
        // deleted from task. In this case delete all files in target that belong to this source.
        // It will be checked during preparing phase. In CSV-file this records have separate format
        // "META|SOURCE|Full path"
        private List<String> oldSources;

        // List of sources in current task that will be saved. Additional hier will be added folders
        // that not removed successfully.
        private List<String> newSources;
         
        // All records in catalog
        private List<CatalogItem> db;
        
        /// <summary>
        /// Create catalog from existing csv-file in ZIP-archive. If ZIP-file not exists create empty 
        /// catalog.
        /// </summary>
        /// <param name="source">path to csv-file the source for catalog</param>
        public Catalog(String catalogDir)
        {
            if(!Directory.Exists(catalogDir))
            {
                throw new Exception("Directory for catalog "+ catalogDir+ " not found.");
            }

            // Contains backed up items in catalog
            db = new List<CatalogItem>();

            // Contains list of sources in catalog
            oldSources = new List<String>();

            // Set path to catalog file and check if exists. If not, catalog stay empty.
            catalogDir.TrimEnd(FS_SPR.ToCharArray());
            pathToCatalogZip = catalogDir + FS_SPR + CATALOG_ZIP;

            // If zip-file exists take catalog from them, else the catalog stay empty.
            // If catalog not exist, assume that backup executed fist time. In this case remove
            // all data in destination folder!
            if (File.Exists(pathToCatalogZip))
            {
                // Rad compressed data from zip-file. Closing resources is not in block finaly {} becuase
                // task end by exection.
                byte[] dataCompressed;
                try
                {
                    long numBytes = new FileInfo(pathToCatalogZip).Length;
                    FileStream fs = new FileStream(pathToCatalogZip, FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    dataCompressed = br.ReadBytes((int)numBytes);
                    br.Close();
                    br.Dispose();
                    fs.Close();
                } catch (Exception e)
                {
                   throw new Exception("Failed read catalog " + pathToCatalogZip + " " + e.Message);
                }

                String dataString = Unzip(dataCompressed);

                // Array of CSV lines
                string[] lines = dataString.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

                // Fill catalog with data from cvs-file
                foreach (string line in lines)
                {
                    // Detect lines with meta data
                    if (line.StartsWith(META))
                    {
                        // Detect lines with sources and add to separate list
                        var metas = line.Split(CSV_SPR);

                        // Detect lines with sources (Format META|SOURCE|Full path) and add to sources list.
                        if (metas.Length > 1 && metas[1].Equals(SOURCE))
                        {
                            oldSources.Add(metas[2]);
                        }
                    }
                    else
                    {
                        var values = line.Split(CSV_SPR);
                        if (values.Length > 3)
                        {
                            CatalogItem item = new CatalogItem(values[0], values[1], values[2], values[3], values[4]);
                            db.Add(item);
                        }
                    }
                }
            } 
            
        }

        /// <summary>
        /// Add a file to catalog based on FileInfo object and set operation "Add".
        /// </summary>
        /// <param name="fi">File-Info object</param>
        public void AddItem(FileInfo fi)
        {
            CatalogItem ci = new CatalogItem(fi, operationType.Add);
            db.Add(ci);
        }

        /// <summary>
        /// Add a directroy to catalog based on DirectroyInfo object and set operation "Add".
        /// </summary>
        /// <param name="di">DirectoryInfo object</param>
        public void AddItem(DirectoryInfo di)
        {
            CatalogItem ci = new CatalogItem(di, operationType.Add);
            db.Add(ci);
        }

        /// <summary>
        /// Add new source to list of sources that will be saved in catalog
        /// </summary>
        /// <param name="newsource">Source path</param>
        public void AddSource(String newsource)
        {
            newSources.Add(newsource);
        }

        /// <summary>
        /// Delete item (set operation Delete). If item is folder on all sub elements set 
        /// operation Deleted. Items with operation Deleted will not be processed, because 
        /// enought delete parent folder.
        /// </summary>
        /// <param name="fullname">Full path of item</param>
        public void DeleteItem(String fullname)
        {
            CatalogItem itemtodelete = GetItemByPath(fullname);
            if (itemtodelete == null) return;
            itemtodelete.optype = operationType.Delete;
            if (itemtodelete.fstype != fsType.Directory) return;

            // By item of type directroy set on all sub-elements operation "Delete"
            CatalogItem[] subitems = Array.FindAll(db.ToArray(), (item) => item.fullname.StartsWith(fullname+ FS_SPR));
            foreach(CatalogItem subitem in subitems)
            {
                subitem.optype = operationType.Deleted;
            }
        }

        /// <summary>
        /// Delete item (set operation Delete). If item is folder on all sub elements set 
        /// operation Deleted. Items with operation Deleted will not be processed, because 
        /// enought delete parent folder.
        /// </summary>
        /// <param name="itemtodelete"></param>
        public void DeleteItem(CatalogItem itemtodelete)
        {
            if (itemtodelete == null) return;
            itemtodelete.optype = operationType.Delete;
            if (itemtodelete.fstype != fsType.Directory) return;

            // By item of type folder set on all sub-elements operation "Deleted"
            CatalogItem[] subitems = Array.FindAll(db.ToArray(), (item) => item.fullname.StartsWith(itemtodelete.fullname + FS_SPR));
            foreach (CatalogItem subitem in subitems)
            {
                subitem.optype = operationType.Deleted;
            }
        }

        /// <summary>
        /// Return false if items are identical, else true. If file not in catalog, add mit 
        /// operation "Add" and return true. If item exists in ctalog, but differ set operation
        /// "Update" and updaten properties of item in catalog and return true. If old is directory
        /// and new is file, by old set operation "Delete" an new set opration "Add" and return true.
        /// </summary>s
        /// <param name="fi">File to compare. No check if it is derectory!</param>
        /// <returns></returns>
        public Boolean CheckFile(FileInfo fi)
        {
            CatalogItem old = GetItemByPath(fi.FullName);

            if (old != null)
            {
                // If old is directory and new is file, set on old operation "Remove" and add new file to catalog with
                // operation "Add".
                if (old.fstype == fsType.Directory)
                {
                    old.optype = operationType.Delete;
                    CatalogItem ci = new CatalogItem(fi, operationType.Add);
                    db.Add(ci);
                    return true;
                }

                // If new file differ from old (in catalog), update properties of existing and set operation flag "Update". 
                if (old.length != fi.Length || old.tscreate != fi.CreationTime.ToString("G") || old.tschange != fi.LastWriteTime.ToString("G"))
                {
                    old.Update(fi);
                    return true;
                }

                return false;
            }
            else
            {
                CatalogItem ci = new CatalogItem(fi, operationType.Add);
                db.Add(ci);
                return true;
            }
        }

        /// <summary>
        /// If directory is in catalog no think to do, return false. If old item is file, set on old operation 
        /// "Delete" and add new item as directroy with operatin "Add" and return true.
        /// </summary>
        /// <param name="di">Object to check</param>
        /// <returns></returns>
        public Boolean CheckDirectory(DirectoryInfo di)
        {
            CatalogItem old = GetItemByPath(di.FullName);

            if (old != null)
            {
                // If old is file, set on old operation Delete and add new one to catalog with operation Add.
                if (old.fstype == fsType.File)
                {
                    old.optype = operationType.Delete;
                    CatalogItem ci = new CatalogItem(di, operationType.Add);
                    db.Add(ci);
                    return true;
                }

                old.Update(di);
                return false;
            }
            else
            {   // Add new directroy to catalog
                CatalogItem ci = new CatalogItem(di, operationType.Add);
                db.Add(ci);
                return true;
            }
        }

        /// <summary>
        /// Check if sources saved in catalog are in the current task. If source in catalog
        /// not in current task, delete this it catalog and all subelememts.
        /// </summary>
        /// <param name="newSources">Array of sources in current task</param>
        public void CheckSources(String[] newSources)
        {
            foreach(String oldsource in oldSources)
            {
                if(!newSources.Contains(oldsource))
                {
                    Logger.Warn("Source "+ oldsource+ " not in current task. All data in backup will be deleted.");
                    DeleteItem(oldsource);
                }
            }
        }

        /// <summary>
        ///  Get item from catalog based on properties FullName (FileInfo or DirectroyInfo). If not found return null.
        /// </summary>
        /// <param name="fullname">Full path</param>
        /// <returns>CatalogItem if found, else null.</returns>
        public CatalogItem GetItemByPath(String fullname)
        {
            return Array.Find(db.ToArray(), (item) => item.fullname.Equals(fullname));
        }

        /// <summary>
        /// Return all items filtered by fstype and by operationType as array.
        /// </summary>
        /// <param name="type">fsType. File-System type of item.</param>
        /// <param name="operation">Operation-type of item.</param>
        /// <returns>Array of catalogItems.</returns>
        public CatalogItem[] FilterAll(fsType type, operationType operation )
        {
            CatalogItem[]  result = Array.FindAll(db.ToArray(), (item) => item.fstype.Equals(type) && item.optype.Equals(operation));
            return result;
        }
        
        /// <summary>
        /// Return all items filtered by operationTyp as array.
        /// </summary>
        /// <param name="operation">Operation type</param>
        /// <returns></returns>
        public CatalogItem[] FilterAll(operationType operation)
        {
            CatalogItem[] result = Array.FindAll(db.ToArray(), (item) => item.optype.Equals(operation));
            return result;
        }

        /// <summary>
        /// Return amount items in catalog
        /// </summary>
        /// <returns>Integer</returns>
        public int Count()
        {
            return db.Count;
        }
        
        /// <summary>
        /// Return item at index. If not found retrun null
        /// </summary>
        /// <param name="index">Intex of item</param>
        /// <returns>CatalogItem</returns>
        public CatalogItem GetItemAt(int index)
        {
            if(index < 0 || index >= db.Count) 
                return null;

            return db.ElementAt(index);
        }

        /*
        /// <summary>
        /// Save catalog to the same csv-file as was it opened. One item per line. Items with 
        /// operation Delete or Deleted don't save, because they are deleted. Items that copyied
        /// with error, has properties reseted to 0. May be by next execution the file will be 
        /// copied correct succesfully.
        /// </summary>
        public void Save(String[] newsources)
        {
            // By saving exclude removed items
            CatalogItem[] tosave = Array.FindAll(db.ToArray(), 
                (item) => item.optype != operationType.Delete && item.optype != operationType.Deleted);

            // Create array of strings to save items in CSV + meta records
            String[]  lines =  new String[tosave.Length + newsources.Length];
            
            // At begin CSV save meta-records (source folders).
            for (int i = 0; i < newsources.Length; i++)
            {
                lines[i] = META+ CSV_SPR + SOURCE+ CSV_SPR+ newsources[i];
            }

            // After meta records add catalog items.
            for (int i=0; i < tosave.Length; i++)
            {
                if (tosave[i].ToString().Length > 3) {
                    lines[i + newsources.Length] = tosave[i].ToString();
                }
            }

            // Save array with all data to file
            File.WriteAllLines(pathToCatalogFile, lines);
        }*/

        /// <summary>
        /// Save catalog to the same ZIP-file as was it opened. At begin save META records. Items with
        /// operation Delete or Deleted don't save, because they are deleted. Items that copyied
        /// with error, has properties reseted to 0. May be by next execution they will be copied
        /// succesfully.
        /// </summary>
        public void SaveToZip(String[] newsources)
        {
            // By saving exclude removed items
            CatalogItem[] tosave = Array.FindAll(db.ToArray(),
                (item) => item.optype != operationType.Delete && item.optype != operationType.Deleted);

            // Create array of strings to save items in CSV + meta records
            String data = "";

            // At begin CSV save meta-records (sources).
            for (int i = 0; i < newsources.Length; i++)
            {
                data += META + CSV_SPR + SOURCE + CSV_SPR + newsources[i] + NEW_LINE;
            }

            // After meta records add catalog items.
            for (int i = 0; i < tosave.Length; i++)
            {
                if (tosave[i].ToString().Length > 3)
                {
                    data += tosave[i].ToString() + NEW_LINE;
                }
            }

            byte[] dataCompressed = Zip(data);

            try
            {
                FileStream fs = new FileStream(pathToCatalogZip, FileMode.Create, FileAccess.Write);
                fs.Write(dataCompressed, 0, dataCompressed.Length);
                fs.Close();
            } catch (Exception e)
            {
                Logger.Error("Exception by saving catalog to "+ pathToCatalogZip+ " ." + e.Message);
                throw new Exception("Exception by saving catalog. " + e.Message);
            }

        }


        // ZIP Example
        // https://sandervandevelde.wordpress.com/2017/12/20/zip-and-unzip-a-string-of-data-in-memory/

        /// <summary>
        /// Compress a string in zipped byte array and return the array.
        /// </summary>
        /// <param name="stringToZip">The string to be zipped.s</param>
        /// <returns>byte[] zipped data</returns>
        public static byte[] Zip(string stringToZip)
        {
            using (var memoryStream = new MemoryStream())
            {
                using (var zipArchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                {
                    var zipFile = zipArchive.CreateEntry(CATALOG_FILE);

                    using (var entryStream = zipFile.Open())
                    {
                        using (var streamWriter = new StreamWriter(entryStream))
                        {
                            streamWriter.Write(stringToZip);
                        }
                    }
                }

                return memoryStream.ToArray();
            }
        }

        /// <summary>
        /// Uncompress a zipped byte array and return as UTF8 string.
        /// </summary>
        /// <param name="zippedData">The byte array to be unzipped</param>
        /// <returns>string representing the original stream</returns>
        public static string Unzip(byte[] zippedData)
        {
            using (var zippedStream = new MemoryStream(zippedData))
            {
                using (var archive = new ZipArchive(zippedStream))
                {
                    var entry = archive.Entries.FirstOrDefault();

                    if (entry != null)
                    {
                        using (var unzippedEntryStream = entry.Open())
                        {
                            using (var ms = new MemoryStream())
                            {
                                unzippedEntryStream.CopyTo(ms);
                                var unzippedArray = ms.ToArray();
                                return Encoding.UTF8.GetString(unzippedArray);
                            }
                        }
                    }

                    return null;
                }
            }
        }

        // End class Catalog
    }
}
