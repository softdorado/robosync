﻿// Copyright (c) 2021, Andrej Koslov. Dustributed under BSD-3 License"

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Robosync
{
    // The class contains only static functions and variables to process syncronisation or backup.
    public static class Syncer
    {
        // Version from projekt properties
        public static readonly String AppVersion = 
            System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

        // Flag to stop synchronization. If true synchronisaton will be stoped automaticaly.
        public static Boolean IsStopping;

        // Flag indicating if synchronizations is running (one or more tasks).
        public static Boolean IsRunning;

        // Flag indicating if task executed in Backup-Mode or Sync-Mode. Should be set before process data.
        public static Boolean isBackupMode = true;

        // Sources of data by Sync-Mode. Set befor process data. For Backup-Mode not used.
        public static String syncSource;

        /*
         * Deprecated! Old way synchronisation without catalog.
         * 
        // For Sync-Mode accepted difference 2 sec in tic. One Windows ticks = 100 nanoces.
        // For user case this programm changed file time difference will be always more as 2 sec.
        private static readonly long acceptedLastWriteTimeDif1 = -20000000;
        private static readonly long acceptedLastWriteTimeDif2 = 20000000;
        */

        /*
         * Deprecated! Old way synchronisation without catalog.
         * 
        /// <summary>
        /// Synchronise directory recursive. If found subdir call self to process subdirectory.
        /// If during execution flag SyncStop is true, cancel execution with exeption.
        /// </summary>
        /// <param name="source">Path to source folder</param>
        /// <param name="dest">PAth to destination folder</param>
        /// <param name="reDir">RegExpresstion to excluder folders</param>
        /// <param name="reFile">RegExprssion to xclude files</param>
        /// <returns>Amount of errors by processing</returns>
        public static int SyncStart(String source, String dest, Regex reDir, Regex reFile)
        {
            // Error counter
            int errorc = 0;

            // Return if source or destination not exists.
            if (!Directory.Exists(source))
            {
                Logger.Error("Source directory \"" + source + "\" not found!");
                return 1;
            }

            if (!Directory.Exists(dest))
            {
                Logger.Error("Destination \"" + dest + "\" not found!");
                return 1;
            }

            String destfs = GetDriveFormat(dest);

            // Always remove trailing back slash in source and destination
            dest = dest.TrimEnd(@"\".ToCharArray());
            source = source.TrimEnd(@"\".ToCharArray());

            DirectoryInfo diSource, diDest;
            FileInfo[] arFilesSource, arFilesDest;
            DirectoryInfo[] arDirsSource, arDirsDest;

            //
            // Synchronization files in source and taget. By exception only log error.
            //

            // Get files in source
            diSource = new DirectoryInfo(source);

            // Skip special windows directory as source.
            if (IsSpecial(diSource))
            {
                Logger.Warn("Skip special directory \"" + source + "\"");
                return 0;
            }

            try
            {
                arFilesSource = diSource.GetFiles();
            }
            catch (System.Exception e)
            {
                Logger.Error("Failed get files in source \"" + source + "\" " + e.Message);
                return 1;
            }

            // Get files in destination
            diDest = new DirectoryInfo(dest);
            try
            {
                arFilesDest = diDest.GetFiles();
            }
            catch (System.Exception e)
            {
                Logger.Error("Failed get files in destination \"" + source + "\" " + e.Message);
                return 1;
            }

            // Variable for result of lookup one file in list. Null if not found.
            FileInfo fileInDest;

            // Used for full path to new file or new directory.
            string newDest;

            // Copy missing or changed files. Skip exluded.
            for (int i = 0; i < arFilesSource.Length; i++)
            {
                // Check if syncronisation should be stoped
                if (IsStopping)
                    throw new InvalidOperationException("Syncronisation is stoped.");

                // Skip excluded
                if (reFile.IsMatch(arFilesSource[i].Name))
                {
                    Logger.Debug("Excluded file " + arFilesSource[i].FullName);
                    continue;
                }

                // Lookup source file in destination list.
                fileInDest = Array.Find(arFilesDest, element => element.Name.Equals(arFilesSource[i].Name));

                // Set full path to new file
                newDest = dest + "/" + arFilesSource[i].Name;

                // If file in destination not found, copy file to destination. If file exist copy only
                // if files are different.
                if (fileInDest == null)
                {
                    Logger.Debug("Copy new file " + arFilesSource[i].FullName);
                    errorc += SafeCopyFileBySync(arFilesSource[i], newDest, destfs);

                    // Update remaining size counter
                    Manager.UpdateStatus(arFilesSource[i].Length);
                }
                else
                {
                    if (FilesAreDifferentBySync(arFilesSource[i], fileInDest))
                    {
                        Logger.Debug("Copy changed file " + arFilesSource[i].Name);
                        errorc += SafeCopyFileBySync(arFilesSource[i], fileInDest.FullName, destfs);

                        // Update remaining size counter
                        Manager.UpdateStatus(arFilesSource[i].Length);
                    }
                }
            }

            // Remove if file in destination don't exist in source or excluded.
            for (int i = 0; i < arFilesDest.Length; i++)
            {
                // Check if syncronisation should be stoped
                if (IsStopping)
                    throw new InvalidOperationException("Syncronisation is stoped.");

                // Skip excluded
                if (reFile.IsMatch(arFilesDest[i].Name))
                {
                    Logger.Debug("Delete excluded file " + arFilesDest[i].FullName);
                    errorc += SafeDeleteFile(arFilesDest[i].FullName);
                    continue;
                }

                // Lookup source file in destination list.
                fileInDest = Array.Find(arFilesSource, element => element.Name.Equals(arFilesDest[i].Name));

                // If file in source not found delete in destination.
                if (fileInDest == null)
                {
                    Logger.Debug("Delete missing file " + arFilesDest[i].FullName);
                    errorc += SafeDeleteFile(arFilesDest[i].FullName);
                }
            }

            //
            // Synchronization directories in source and taget. By exception return error.
            //

            // Get directories in source
            try
            {
                arDirsSource = diSource.GetDirectories();
            }
            catch (System.Exception e)
            {
                Logger.Error("Failed get directories in source \"" + source + "\" " + e.Message);
                return errorc + 1;
            }

            // Get directories in destination
            try
            {
                arDirsDest = diDest.GetDirectories();
            }
            catch (System.Exception e)
            {
                Logger.Error("Failed get directories in destination \"" + source + "\" " + e.Message);
                return errorc + 1;
            }

            // Variable for result of lookup one directory in list. Null if not found.
            DirectoryInfo result2;

            // Sync directories. Skip exluded.
            for (int i = 0; i < arDirsSource.Length; i++)
            {
                // Check if syncronisation should be stoped
                if (IsStopping)
                    throw new InvalidOperationException("Syncronisation is stoped.");

                // Skip excluded directories
                if (reDir.IsMatch(arDirsSource[i].Name))
                {
                    Logger.Debug("Excluded directory \"" + arDirsSource[i].FullName + "\"");
                    continue;
                }

                // Skip if directory is special windows directory.
                if (IsSpecial(arDirsSource[i]))
                {
                    Logger.Warn("Skip special directory \"" + arDirsSource[i].FullName + "\"");
                    continue;
                }

                // Lookup source directory in destination list.
                result2 = Array.Find(arDirsDest, element => element.Name.Equals(arDirsSource[i].Name));

                // If directory in destination not found create new. After that call function to sync directory.
                if (result2 == null)
                {
                    // Try create directory in destination and update destination to copy there data from source.
                    newDest = dest + @"\" + arDirsSource[i].Name;

                    try
                    {
                        if (!Directory.Exists(newDest))
                        {
                            Logger.Debug("Create direcory \"" + newDest + "\"");
                            Directory.CreateDirectory(newDest);
                        }
                    }
                    catch
                    {
                        Logger.Error("Failed create direcory \"" + newDest + "\"");
                        return errorc + 1;
                    }

                    // Sync new directroy.
                    errorc += SyncStart(arDirsSource[i].FullName, newDest, reDir, reFile);
                }
                else
                {
                    // Syn existieng directroy.
                    errorc += SyncStart(arDirsSource[i].FullName, result2.FullName, reDir, reFile);
                }
            }

            // Remove if directory in destination don't exist in source or excluded.
            for (int i = 0; i < arDirsDest.Length; i++)
            {
                // Check if syncronisation should be stoped
                if (IsStopping)
                    throw new InvalidOperationException("Syncronisation is canceled.");

                // Skip excluded directories
                if (reDir.IsMatch(arDirsDest[i].Name))
                {
                    Logger.Debug("Delete excluded directory \"" + arDirsDest[i].FullName + "\"");
                    errorc += SafeDeleteDir(arDirsDest[i].FullName);
                    continue;
                }

                // Lookup source file in destination list.
                result2 = Array.Find(arDirsSource, element => element.Name.Equals(arDirsDest[i].Name));

                // If directory in source not found delete in destination. If directory deleting
                // failed program will be abortet. Example if directrory contains read-only files,
                // deleting failed. I special don't catch this error.
                if (result2 == null)
                {
                    Logger.Debug("Delete missing directroy \"" + arDirsDest[i].FullName + "\"");
                    errorc += SafeDeleteDir(arDirsDest[i].FullName);
                }
            }

            return errorc;
        }
        */

        /*
         * Deprecated! Old way synchronisation without catalog.
         * 
        /// <summary>
        /// Return overall size of data that would be transferd by synchronisation in bytes, consider
        /// excluded directories and files. If source is special windows directory return 0. Exception
        /// will not be catched!
        /// </summary>
        /// <param name="source">Path to source</param>
        /// <param name="dest">Path to destination</param>
        /// <param name="reDir">Regular expression for excluded directories</param>
        /// <param name="reFile">Regular expression for excluded files.</param>
        /// <returns>Expected size of tranfered data by synchronisation.</returns>
        public static long SyncGetSize(string source, string dest, Regex reDir, Regex reFile)
        {
            long syncsize = 0;

            // Return 0 if source not exists.
            if (!Directory.Exists(source)) return 0;

            // Always remove trailing back-slash in source and destination
            dest.TrimEnd(@"\".ToCharArray());
            source.TrimEnd(@"\".ToCharArray());

            // Source and destination as DirectroyInfo
            DirectoryInfo diSource = new DirectoryInfo(source);
            DirectoryInfo diDest = new DirectoryInfo(dest);

            // Array of files as FileInfo in source and destination
            FileInfo[] arFilesSource = diSource.GetFiles();

            // Array of files in destination. If destination not exist or some other case,
            // exception occure in this case set epty array.
            FileInfo[] arFilesDest;

            if (diDest.Exists)
            {
                try
                {
                    arFilesDest = diDest.GetFiles();
                }
                catch
                {
                    arFilesDest = new FileInfo[] { };
                }
            }
            else
            {
                arFilesDest = new FileInfo[] { };
            }

            // Return 0 if source is special windows directory.
            if (IsSpecial(diSource)) return 0;

            // Variable for result of lookup one file in list. Null if not found.
            FileInfo result1;

            // Used for full path to new file or new directory.
            string newDest;

            // Calculate size of synchronisation by transfer missing or changed files. Skip exluded.
            for (int i = 0; i < arFilesSource.Length; i++)
            {
                // Skip excluded
                if (reFile.IsMatch(arFilesSource[i].Name)) continue;

                // Lookup source file in destination list.
                result1 = Array.Find(arFilesDest, element => element.Name.Equals(arFilesSource[i].Name));

                // If file in destination not found or files are different add size of file to syncsize.
                if (result1 == null || FilesAreDifferentBySync(arFilesSource[i], result1))
                {
                    syncsize += arFilesSource[i].Length;
                }
            }

            // Add size by synchronisatoin of directories

            // Array of firectories as DirectroyInfo for source
            DirectoryInfo[] arDirsSource = diSource.GetDirectories();

            // Calculate size of transfered data by synchronisation sub-directories. Skip excluded
            // and Windows special one.
            for (int i = 0; i < arDirsSource.Length; i++)
            {
                // Skip excluded directories
                if (reDir.IsMatch(arDirsSource[i].Name)) continue;

                // Skip special windows directory.
                if (IsSpecial(arDirsSource[i])) continue;

                // Set path to "new" directroy in destination
                newDest = dest + @"\" + arDirsSource[i].Name;

                // Get difference current directroy in source and destination
                syncsize += SyncGetSize(arDirsSource[i].FullName, newDest, reDir, reFile);
            }

            return syncsize;
        }

        /// <summary>
        /// Check if two files are different. Detection per last-write-time don't works properly.
        /// If files on different file system (NTFS, FAT23, ... Samba), due to this problem I
        /// tolerate difference 2 sec.
        /// </summary>
        /// <param name="file1">First file to compare.</param> 
        /// <param name="file2">Secon file to compare.</param>
        /// <returns>return true if files are different, else flse.</returns>
        private static Boolean FilesAreDifferent(FileInfo file1, FileInfo file2)
        {
            if (file1.Length != file2.Length)
                return true;

            // If source and destination on different file-system (NTFS and FAT32) this check
            // works only as string!
            if (file1.CreationTime.ToString() != file2.CreationTime.ToString())
                return true;

            // Last-write-time on NTFS and on FAT32 are not the exact the same. I decided accept
            // difference till 2 seconds. Two files are "not different" if last-write-time not more
            // as 2 seconds. 
            long dif = (file2.LastWriteTime.Ticks - file1.LastWriteTime.Ticks);

            // dif may be positive or negative! Return true only if differenze is outsite Dif1 - Dif2. 
            if (dif > acceptedLastWriteTimeDif2 || dif < acceptedLastWriteTimeDif1)
                return true;

            return false;
        }
        */

        /// <summary>
        /// Synchronise directory recursive. If found subdir call self to process subdirectories. 
        /// If during execution flag SyncStop is true (user may change it), cancel execution 
        /// with ecxeption.
        /// </summary>
        /// <param name="dest">String. Path to destination where to save data.</param>
        /// <param name="reDir">Regex. Regexpr for exluded directries</param>
        /// <param name="reFile">Regex. Regexpr for exluded directries</param>
        /// <param name="catalog">Catalog. Catalog is database about data in destination.</param>
        /// <returns>Integer. Amount of errors by execution.</returns>
        public static int BackupStart(String dest, Regex reDir, Regex reFile, Catalog catalog)
        {
            // Error counter
            int errorc = 0;
            
            //Gets the name of the filesystem on destination, such as NTFS or FAT32.
            String destfs = GetDriveFormat(dest);

            // The string contains real path to destination based on path of processed item and
            // configured path to destination and processinf Mode BAckup or Sync.
            // Example Backup-Mode 
            // Source=C:\Temp\Data, Item C:\Temp\Data\Example, Destination=\\Server\Backup -> pathInDest=\\Server\Backup\C\Temp\Data\Example
            // Example Sync -Mode
            // Source=C:\Temp\Data, Item C:\Temp\Data\Example, Destination=\\Server\Sync\ -> pathInDest=\\Server\Sync\Example
            String pathInDest;

            // Nexts steps:
            // 1 Delete not existing files in target.
            // 2 Delete not existing directories in targed.
            // 3 Create new directroy in target.
            // 4 Copy new files to target.
            // 5 Copy changed files to target.

            // 1 Delete files in target
            CatalogItem[] files = catalog.FilterAll(fsType.File, operationType.Delete);
            foreach (CatalogItem item in files)
            {
                pathInDest = PathToDestination(item.fullname, dest);

                // If failed, reset properies to repeat this opration next time.
                if (SafeDeleteFile(pathInDest) > 0)
                {
                    item.SetOperationFailed(); 
                    errorc++;
                }

                if (IsStopping)
                    throw new InvalidOperationException("Syncronisation is stoped.");
            }
            files = null;

            // 2 Delete folders in targed
            foreach (CatalogItem item in catalog.FilterAll(fsType.Directory, operationType.Delete))
            {
                pathInDest = PathToDestination(item.fullname, dest);

                // If failed, reset properies to repeat this opration next time.
                if (SafeDeleteDir(pathInDest) > 0)
                {
                    item.SetOperationFailed();
                    errorc++;
                }

                if (IsStopping)
                    throw new InvalidOperationException("Syncronisation is stoped.");
            }

            // 3 Create new folders in target.
            foreach (CatalogItem item in catalog.FilterAll(fsType.Directory, operationType.Add))
            {
                pathInDest = PathToDestination(item.fullname, dest);

                // If failed, reset properies to repeat this opration next time.
                if (SafeCreateDir(pathInDest) > 0)
                {
                    item.SetOperationFailed();
                    errorc++;
                }

                if (IsStopping)
                    throw new InvalidOperationException("Syncronisation is stoped.");
            }

            // 4 Copy new files to target.
            foreach (CatalogItem item in catalog.FilterAll(fsType.File, operationType.Add))
            {
                pathInDest = PathToDestination(item.fullname, dest);

                // Copy file. If failed, reset properies.
                if(SafeCopyFile(item.fullname, pathInDest, destfs) > 0)
                {
                    item.SetOperationFailed();
                    errorc++;
                }

                // Update status of task.
                Manager.UpdateStatus(item.length);

                if (IsStopping)
                    throw new InvalidOperationException("Syncronisation is stoped.");
            }

            // 5 Copy changed files to target.
            foreach (CatalogItem item in catalog.FilterAll(fsType.File, operationType.Update))
            {
                pathInDest = PathToDestination(item.fullname, dest);

                // Copy file. If failed, reset properies.
                if (SafeCopyFile(item.fullname, pathInDest, destfs) > 0)
                {
                    item.SetOperationFailed();
                    errorc++;
                }

                // Update status of task.
                Manager.UpdateStatus(item.length);

                if (IsStopping)
                    throw new InvalidOperationException("Syncronisation is stoped.");
            }
            
            return errorc;
        }

        /// <summary>
        /// Based on data in catalog calculate overall size of data that would be transferd by 
        /// synchronisation in bytes. Consider excluded directories and files. 
        /// <summary>
        /// <param name="sources">List of sources in the task</param>
        /// <param name="reDir">Regular expression for excluded directories.</param>
        /// <param name="reFile">Regular expression for excluded files.</param> 
        /// <param name="catalog">Catalog. Backup catalog.</param>
        /// <paramref name="dest">Path to backup destination</param>
        /// <returns>Long. Size transfered data in bytes.</returns>
        public static long BackupPrepare(String[] sources, Regex reDir, Regex reFile, Catalog catalog, String dest)
        {
            // Size of files that must be transfered
            long allsize = 0;

            // If catalog in destination not exists, it look like backup executed first time. In this case
            // remove all data in destination.
            try
            {
                if (!File.Exists(catalog.pathToCatalogZip))
                {
                    Logger.Warn("Folder for backup contains unknown data. Tray delete all unknown data.");
                    DirectoryInfo diDest = new DirectoryInfo(dest);

                    // Delete all files
                    FileInfo[] filesToDelete = diDest.GetFiles();
                    foreach (FileInfo file in filesToDelete)
                    {
                        Logger.Warn("Delete unknown file "+ file.FullName);
                        file.Delete();
                    }

                    // Delete all folders
                    DirectoryInfo[] foldersToDelete =  diDest.GetDirectories();
                    foreach (DirectoryInfo folder in foldersToDelete)
                    {
                        Logger.Warn("Delete unknow folder " + folder.FullName);
                        folder.Delete(true);
                    }
                }
            } catch( Exception e)
            {
                Logger.Error("Some error occure by deleting unknow data in destination folder.");
                Logger.Error("Error message. "+ e.Message);
                throw new Exception("Error occured by deleting unknow data in destination folder.");
            }

            // Detect new and changed items and set operation Add or Update.
            foreach (String source in sources)
            {
                allsize += PrepareDirectory(source, reDir, reFile, catalog);
            }

            // Check which items in catalog are not in sources and set operation Delete. This
            // happen if source (folder) deleted in configuration of task.
            catalog.CheckSources(sources);

            // Detect deleted items and set operation Delete. Skip items that already marked to delete
            // oder Deleted (case <deleted sub-element)
            CatalogItem item;
            for (int i = 0; i < catalog.Count(); i++)
            {
                item = catalog.GetItemAt(i);
                if (item.optype != operationType.Delete && item.optype != operationType.Deleted) 
                {
                    if (item.fstype == fsType.Directory && !Directory.Exists(item.fullname))
                    {
                        catalog.DeleteItem(item);
                    }
                    else if (item.fstype == fsType.File && !System.IO.File.Exists(item.fullname))
                    {
                        catalog.DeleteItem(item);
                    }
                }
            }

            return allsize;
        }
        
        /// <summary>
        /// Prepare one directroy in source for synchronisation and return size of data that should
        /// be transfered in bytes. Consider excluded directories and files. If source is 
        /// special windows directory return 0. Exception will not be catched! By new files 
        /// in catalog set operation "Add". By changed operation "Update". By new directroy 
        /// operation "Add". By not existing files or directories set flag "Delete".
        /// <summary>
        /// <param name="source">String. Path to source.</param>
        /// <param name="reDir">Regular expression for excluded directories.</param>
        /// <param name="reFile">Regular expression for excluded files.</param> 
        /// <param name="catalog">Catalog. Backup catalog.</param>
        /// <returns>Long. Size transfered data.</returns>
        private static long PrepareDirectory(String source, Regex reDir, Regex reFile, Catalog catalog)
        {
            // Size of files in the directroy that must be transfered
            long dirsize = 0;
            
            // If source not exist, delete directroy and all sub-element in catalog and return 0.
            if (!Directory.Exists(source))
            {
                Logger.Error("Source directory "+ source+ " not found! Data in backup will be deleted.");
                catalog.DeleteItem(source);
                return 0;
            }

            // Always delete trailing back-slash in source
            source.TrimEnd(@"\".ToCharArray());

            // Source as DirectroyInfo
            DirectoryInfo diSource = new DirectoryInfo(source);

            // Return 0 if source is special windows directory.
            if (IsSpecial(diSource)) return 0;

            // Check if source directroy is in catalog. Case if source is empty.
            catalog.CheckDirectory(diSource);

            // Array of files in source
            FileInfo[] arFilesSource = diSource.GetFiles();

            // Calculate size new or changed files. Skip exluded. New files
            // add in catalog with operation Add. Changed with operation Update.
            for (int i = 0; i < arFilesSource.Length; i++)
            {
                // Skip excluded
                if (reFile.IsMatch(arFilesSource[i].Name))
                {   
                    Logger.Debug("Skip file "+ arFilesSource[i].FullName);
                    continue;
                }

                // New file add to catalog with operation "Add". If file differ set operation "Update"
                if(catalog.CheckFile(arFilesSource[i]))
                {
                    dirsize += arFilesSource[i].Length;
                }
            }
                
            // Array of directories in source
            DirectoryInfo[] arDirsSource = diSource.GetDirectories();

            // Process sub-directories in source. Skip excluded and Windows special one.
            for (int i = 0; i < arDirsSource.Length; i++)
            {
                // Skip excluded directories
                if (reDir.IsMatch(arDirsSource[i].Name))
                {
                    Logger.Debug("Skip directroy "+ arDirsSource[i].FullName);
                    continue;
                }

                // Skip special windows directory.
                if (IsSpecial(arDirsSource[i]))
                {
                    Logger.Debug("Skip special windows directory " + arDirsSource[i].FullName);
                    continue;
                }

                // Check if directory is in catalog. If not all sub-elements add to catalog
                // without check. Else call standard function for processing directories.
                if (catalog.CheckDirectory(arDirsSource[i]))
                {
                    dirsize += DirectoryAddToCatalog(arDirsSource[i], reFile, reDir, catalog);
                } else {
                    dirsize += PrepareDirectory(arDirsSource[i].FullName, reDir, reFile, catalog);
                }
            }

            if (IsStopping)
                throw new InvalidOperationException("Syncronisation is stoped.");
            
            return dirsize;
        }

        /// <summary>
        /// Add all sub-elements of directory without check in catalog. Skip excluded elemens.
        /// </summary>
        /// <param name="diNew">Full path to parent directroy</param>
        /// <param name="reDir">Regular expression for excluded directories.</param>
        /// <param name="reFile">Regular expression for excluded files.</param> 
        /// <param name="catalog">Catalog. Backup catalog.</param>
        /// <returns>Long. Size transfered data.</returns>
        internal static long DirectoryAddToCatalog(DirectoryInfo diNew, Regex reFile, Regex reDir, Catalog catalog)
        {
            //Logger.Debug("New dir "+ diNew.FullName);
            long syncsize = 0;

            // Array of files as FileInfo in new directroy
            FileInfo[] arFiles = diNew.GetFiles();

            // Files in directroy add to catalog, but skip exluded.
            foreach(FileInfo fi in arFiles)
            {
                if (reFile.IsMatch(fi.Name))
                {
                    Logger.Debug("Skip file "+ fi.FullName);
                    continue;
                }
                catalog.AddItem(fi);
                syncsize += fi.Length;
                //Logger.Debug("New file "+ fi.FullName);
            }

            // Array of directories as DirectoryInfo in new directroy 
            DirectoryInfo[] arDirs = diNew.GetDirectories();

            // Subdirectory prozess with the same function, but skip excluded.
            foreach (DirectoryInfo di in arDirs)
            {
                if (reDir.IsMatch(di.Name))
                {
                    Logger.Debug("Skip directory "+ di.FullName);
                    continue;
                }
                catalog.AddItem(di);
                syncsize += DirectoryAddToCatalog(di, reFile, reDir, catalog);
            }
            
            return syncsize;
        }

        /// <summary>
        /// Create path for destination based on source path of procesed item.
        /// In Sync-Mode or Backup-Mode logic is different! 
        /// 
        /// Example Backup-Mode:
        /// Processed item     : C:\UserProfile\Username
        /// Destination path   : X:\Backup
        /// Result path        : X:\Backup\C\UserProfile\Username
        /// 
        /// Example Sync-Mode:
        /// Source path     : C:\Archive
        /// Processed item  : C:\Archive\Photos
        /// Destination path: X:\MyData\Archive
        /// Result path     : X:\MyData\Archive\Photos
        /// 
        /// </summary>
        /// <param name="destPath">Destination path</param>
        /// <param name="sourcePath">Source path</param>
        /// <returns>String</returns>
        private static String PathToDestination(String sourcePath, String destPath)
        {
            destPath.TrimEnd(Catalog.FS_SPR.ToCharArray());
            
            if (isBackupMode)
            {
                sourcePath = sourcePath.Replace(":", "");
                destPath += Catalog.FS_SPR + sourcePath;

            } else {
                destPath += sourcePath.Remove(0, syncSource.Length);
            }
            
            return destPath;
        }

        // Check if file or directory is special windows file/directory. 
        public static Boolean IsSpecial(DirectoryInfo di)
        {
            if (di.Attributes.HasFlag(FileAttributes.System) ||
                    di.Attributes.HasFlag(FileAttributes.ReparsePoint) ||
                        di.Attributes.HasFlag(FileAttributes.Offline))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Delete directory recursive and catch error.
        /// </summary>
        /// <param name="todelete">String. Path to delete </param>
        /// <returns>Integer. 1 by error, else 0.</returns>
        public static int SafeDeleteDir(String todelete)
        {
            int err = 0;

            if (System.IO.Directory.Exists(todelete))
            {
                try
                {
                    Directory.Delete(todelete, true);
                    Logger.Debug("Directory delete ok "+ todelete+ " .");
                }
                catch (System.Exception e)
                {
                    err = 1;
                    Logger.Error("Directory delete failed " + todelete + " . " + e.Message);
                }
            }
            else
            {
                Logger.Debug("Directory to delete not found "+ todelete+ " .");
            }

            return err;
        }
        
        /// <summary>
        /// Create directroy and catch all error.
        /// </summary>
        /// <param name="tocreate">String. Path to new directroy</param>
        /// <returns>Integer. 1 by error, else 0.</returns>
        public static int SafeCreateDir(String tocreate)
        {
            int err = 0;
            
            if (!System.IO.Directory.Exists(tocreate))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(tocreate);
                    Logger.Debug("Directory create ok " + tocreate+ " .");
                }
                catch (System.Exception e)
                {
                    err = 1;
                    Logger.Error("Directory create failed "+ tocreate + " . " + e.Message);
                }
            }
            else
            {
                Logger.Debug("Directory already exists "+ tocreate+ " .");
            }

            return err;            
        }

        /// <summary>
        /// Delete file and catch all error.
        /// </summary>
        /// <param name="todelete">String. Path to file to delete.</param>
        /// <returns>Integer. 1 by error, else 0.</returns>
        public static int SafeDeleteFile(String todelete)
        {
            int err = 0;

            if (System.IO.File.Exists(todelete))
            {
                try
                {
                    System.IO.File.Delete(todelete);
                    Logger.Debug("File delete ok "+ todelete+ " .");
                }
                catch (System.Exception e)
                {
                    err = 1;
                    Logger.Error("File delete failed "+ todelete+ " . "+ e.Message);
                }
            }
            else
            {
                Logger.Debug("File already deleted "+ todelete+ " .");
            }

            return err;
        }

        /*
         * Deprecated! Old way synchronisation without catalog.
         * 
        // Copy file and set creation time as original(overwrite if destination exists) and catch error.
        // If file has flag read-only, remove this flag on targe, because it is unpossible update this
        // file or remove. On FAT32 max file size 4GB (4294967296 Bytes)
        // @param from string - is full path to source file.
        // @param to string - full path to destination file.
        // @param destfs string - type of filesystem on destination FAT32, extFAT, NTFS, ... 
        // @return int - 1 by error, else 0.
        public static int SafeCopyFileBySync(FileInfo from, string to, string typefs)
        {
            if (typefs == "FAT32" && from.Length > 4294967296)
            {
                Logger.Warn("Skip file \"" + from.FullName + "\" Size exceed max allowed size 4GB on FAT32.");
                return 0;
            }

            // Try copy file
            try
            {
                File.Copy(from.FullName, to, true);
            }
            catch (System.Exception e)
            {
                Logger.Error("Failed copy file \"" + from + "\" " + e.Message);
                return 1;
            }

            // If exists remove read-only flag. It need because set creation time on read-only
            // files will failed.
            try
            {
                if (from.IsReadOnly)
                {
                    Logger.Debug("Remove read-only flag \"" + to + "\"");
                    File.SetAttributes(to, FileAttributes.Normal);
                }
            }
            catch (System.Exception e)
            {
                Logger.Error("Failed remove read-only flag on \"" + to + "\" " + e.Message);
                return 1;
            }

            // Set last write time on new file.
            try
            {
                File.SetLastWriteTime(to, from.LastWriteTime);
            }
            catch (System.Exception e)
            {
                Logger.Error("Failed set last write time time on \"" + to + "\" " + e.Message);
                return 1;
            }

            try
            {
                File.SetCreationTime(to, from.CreationTime);
            }
            catch (System.Exception e)
            {
                Logger.Error("Failed set creation time on \"" + to + "\" " + e.Message);
                return 1;
            }

            return 0;
        }
        */

        /*
         * Deprecated! Old way synchronisation without catalog.
         * 
        // Check if two files are different. Detection per last-write-time don't works properly
        // if files on different file system (NTFS, FAT23, ... Samba), due to this problem I
        // tolerate difference 2 sec.
        // @return true if files are different, else flse.
        public static Boolean FilesAreDifferentBySync(FileInfo file1, FileInfo file2)
        {
            if (file1.Length != file2.Length)
                return true;

            // If source and destination on different file-system (NTFS and FAT32) this check
            // works only as string!
            if (file1.CreationTime.ToString() != file2.CreationTime.ToString())
                return true;

            // Last-write-time on NTFS and on FAT32 are not the exact the same. I decided accept
            // difference till 2 seconds. Two files are "not different" if last-write-time not more
            // as 2 seconds. 
            long dif = (file2.LastWriteTime.Ticks - file1.LastWriteTime.Ticks);

            // dif may be positive or negative! Return true only if differenze is outsite Dif1 - Dif2. 
            if (dif > acceptedLastWriteTimeDif2 || dif < acceptedLastWriteTimeDif1)
                return true;

            return false;
        }
        */

        /// <summary>
        /// Copy file and catch all errosr. On FAT32 max file size 4GB (4294967296 Bytes)
        /// </summary>
        /// <param name="from">String. Full path to source file</param>
        /// <param name="to">String. Full path to destination file.</param>
        /// <param name="typefs">String. Type of filesystem on destination FAT32, extFAT, NTFS, ... </param>
        /// <returns>Integer. 1 by error, else 0</returns>
        public static int SafeCopyFile(String from, String to, String destFs)
        {
            FileInfo fromFi = new FileInfo(from);

            if (destFs == "FAT32" && fromFi.Length > 4294967296)
            {
                Logger.Warn("File copy skip "+ from + ". Size exceed max allowed size 4GB on FAT32.");
                return 0;
            }

            // Try copy file
            try
            {
                System.IO.File.Copy(from, to, true);
                Logger.Debug("File copy ok "+ from+ " .");
            }
            catch (System.Exception e)
            {
                Logger.Error("File "+ from+ " copy failed. " + e.Message);
                return 1;
            }

            return 0;
        }

        /// <summary>
        /// Return name of file-system (DriveFormat) of driver ( disk) on some ful
        /// </summary>
        /// <param name="path">String. Full path.</param>
        /// <returns>String. NTFS or FAT32, etc. UNKNOW if drive from path not detected 
        /// as current available driver. E.g full path on network path.
        ///</returns>
        public static String GetDriveFormat(String path)
        {
            DriveInfo[] allDrives = DriveInfo.GetDrives();
            String driveToCheck = path.Split('\\')[0] + "\\";

            foreach (DriveInfo d in allDrives)
            {
                if (d.IsReady == true)
                {
                    if (d.Name == driveToCheck)
                        return d.DriveFormat;
                }
            }

            return "UNKNOWN";
        }
    }
}
