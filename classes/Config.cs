﻿// Copyright (c) 2021, Andrej Koslov. Dustributed under BSD-3 License"

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
//using System.Collections.Generic;

namespace Robosync
{
    // The class contains config for RoboSync.
    [Serializable, XmlRoot("Config")]
    public class Config
    {
        // List of tasks. For automaticaly update GUI element and data-sourceneed need
        // special class ObservableCollection<T>. 
        // See more https://docs.microsoft.com/de-de/dotnet/api/system.collections.objectmodel.observablecollection-1?view=netframework-4.7.2
        // This class exist in .Net Core and .Net Framrwork. For WindowForms for the same
        // functionality need BindingList<T>
        [XmlElement(ElementName = "Task")]
        public ObservableCollection<SyncTask> Task { get; set; }

        public Config ()
        {
            //this.Task = new ();
            this.Task = new ObservableCollection<SyncTask>();
        }
    }

    // The class for synchronisation task.
    [Serializable]
    public class SyncTask
    {
        // The ID is uniq inside the plan, where the task belong
        [XmlElement(DataType = "int", ElementName = "ID")]
        public int ID { get; set; }

        // Logical name for the task
        [XmlElement(DataType = "string", ElementName = "Name")]
        public String Name { get; set; }

        // List of pathes to process. By type Backup may be many. By type syncronisatoin only one.
        [XmlElement(ElementName = "Source")]
        public List<SourceItem> Source { get; set; }

        // Path where to save data
        [XmlElement(DataType = "string", ElementName = "Destination")]
        public String Destination { get; set; }

        // Encrypted username for authentification if destination is network share. If empty or null no authentification.
        // Used to save data to configuration file, but never is visible.
        [XmlElement(DataType = "string", ElementName = "AuthUser")]
        public String AuthUser { get; set; }

        // Plain text username for authentification if destination is network share. If empty or null no authentification.
        // Used in window task-properties.
        [XmlIgnore]
        public String AuthUserPlain { get; set; }

        // Encrypted password for authentification if destination is network share. If empty or null no authentification.
        // Used to save data to confiuration file, but never is visible.
        [XmlElement(DataType = "string", ElementName = "AuthPwd")]
        public String AuthPwd { get; set; }

        // Plain text password for authentification if destination is network share. If empty or null no authentification.
        // Used in window task-properties.
        [XmlIgnore]
        public String AuthPwdPlain { get; set; }

        // In destination directory create structur as in source. Synonym for "backup mode" = true
        // if false ,task run in "synchronisation mode".
        [XmlElement(DataType = "boolean", ElementName = "CreateStructur")]
        public Boolean CreateStructur { get; set; }

        [XmlElement(DataType = "string", ElementName = "XDir")]
        public String XDir { get; set; }

        [XmlElement(DataType = "string", ElementName = "XFile")]
        public String XFile { get; set; }

        [XmlElement(DataType = "string", ElementName = "Status")]
        public String Status { get; set; }

        // Logleve. Possible values
        // INFO log INFO,ERROR, WARN
        // WARN log ERROR, WARN - Default.
        // ERROR log ERROR
        // DEBUG log all
        [XmlElement(DataType = "string", ElementName = "Loglevel")]
        public String Loglevel { get; set; }

        public SyncTask()
        {
        }

        public SyncTask(int ID, String Name, List<SourceItem> Source, String Destination, Boolean CreateStructur, String XDir, String XFile, String Loglevel)
        {
            this.ID = ID;
            this.Name = Name;
            this.Source = Source;
            this.Destination = Destination;
            this.CreateStructur = CreateStructur;
            this.XDir = XDir;
            this.XFile = XFile;
            this.Loglevel = Loglevel;
            Status = "Waiting";
        }
    }

    // Instead simple string need object because, DataGrid.DataSource need item with a properie.
    [Serializable]
    public class SourceItem
    {
        [XmlElement(DataType = "string", ElementName = "Path")]
        public string Path { get; set; }

        public SourceItem()
        {

        }

        public SourceItem(string Path)
        {
            this.Path = Path;
        }

    }

}
