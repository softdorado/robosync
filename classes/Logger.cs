﻿// Copyright (c) 2021, Andrej Koslov. Dustributed under BSD-3 License

using System;
using System.IO;
using System.Windows; // Aplication.CurrentDispatcher
using System.Collections.Generic;
using System.Diagnostics;
using System.ComponentModel; // ClosingEventHandle
using Microsoft.Win32; // For OpenFileDialog

namespace Robosync
{
    // The class contains only static functions for logging. All messages loger write in file
    // per NLog and add to log-viewer. The window of log-viewer default closed, but from main
    // window may be showed. To init logging first should be called function InitLogger();
    public class Logger
    {
        // https://github.com/nlog/nlog/wiki
        // https://github.com/NLog/NLog/wiki/File-target
        private static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        // Dictionary for translation loglevel-string to index.
        public static readonly Dictionary<string, int> loglevelIndex = new Dictionary<string, int>()
        {
            {"ERROR", 0},
            {"WARN",  1},
            {"INFO",  2},
            {"DEBUG", 3}
        };

        // Log viewer. The object defined hier but running in separate thread for GUI!
        private static WindowLogviewer wiViewer;

        // Flag indicate if viewer was activated. Need separate as variabe becasue window.isActiv from
        // another threads is not accessible.
        private static Boolean isViewerActive = false;

        // Action used to append log message to viewer per delegate. It need because window log-viewer
        // is running in thread for GUI, but add request initiated from another thread.
        private static Action actionAddLogMsg;

        // Loglevel. Initial 2
        private static int loglevel = 2;

        // This function should be caled before use other logging functions
        public static void InitLogger ()
        {
            Trace.AutoFlush = true;

            // Create instance of form log-viewer and add events hanlder.
            if (wiViewer == null)
            {
                wiViewer = new WindowLogviewer();
                wiViewer.Closing += new CancelEventHandler(ViewerClose);
            }
        }

        // Set loglevel using string. Used if loglevel defined task as string.
        public static void SetLoglevel (string level)
        {
            loglevel = loglevelIndex[level];
        }

        // Set default loglevel. Need to log normal operations.
        public static void SetDefaultLogLevel()
        {
            loglevel = 2;
        }

        // Catch close-window event and only hide log-viewer window and cancel event.
        private static void ViewerClose(object sender, CancelEventArgs e)
        {
            wiViewer.Hide();
            isViewerActive = false;
            e.Cancel = true;
        }

        // Show log-viewer window. Called from main window.
        public static void ViewerShow()
        {
            if (!isViewerActive)
            {
                wiViewer.Show();
                isViewerActive = true;
            }
        }

        // Add new line to log-viewer. It works always if window is visible or not. Line added
        // not directly because this function may be calles from another thread. To add line
        // from another thread need Action for Delegate. 
        private static void ViewerAddLine(String line)
        {
            if (wiViewer == null) return;

            // Create Action as Lambda expression. Instead Lambda may be used separate name of function.
            // See http://dotnetpattern.com/csharp-action-delegate
            // Example  one line "actionAddLogMsg = () => wiViewer.rtLog.Inlines.Add(line);"
            actionAddLogMsg = () =>
            {
                wiViewer.rtLog.Inlines.Add(line+ "\n");
                wiViewer.rtBox.ScrollToEnd();
            };

            Application.Current.Dispatcher.Invoke(actionAddLogMsg);
        }

        // Select logfile and show content in log-viewer
        public static void OpenLogfile()
        {
            // From Microsoft.Win32,
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = Environment.CurrentDirectory;
             
            if (dialog.ShowDialog() == true)
            {
                wiViewer.rtLog.Inlines.Clear();
                foreach (String line in File.ReadLines(dialog.FileName))
                {
                    wiViewer.rtLog.Inlines.Add(line + Environment.NewLine);
                }
                wiViewer.rtBox.ScrollToEnd();
            }
        }

        /// <summary>
        /// Return current data-time as preformated string.
        /// </summary>
        /// <returns>String</returns>
        private static String GetFormatedTimeNow()
        {
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        // Output debug message if loglevel suitable.
        public static void Debug(string msg)
        {
                if (loglevel < loglevelIndex["DEBUG"])
                return;

            logger.Debug(msg);
            msg = GetFormatedTimeNow() + "|DEBUG|" + msg;
            ViewerAddLine(msg);

            //#if DEBUG
            //    Trace.WriteLine("DEBUG|"+ msg);
            //#endif
        }

        // Output info message if loglevel suitable.
        public static void Info(string msg)
        {
            if (loglevel < loglevelIndex["INFO"])
                return;

            logger.Info(msg);
            msg = GetFormatedTimeNow()+ "|INFO|"+ msg;
            ViewerAddLine(msg);
            
            //#if DEBUG
            //    Trace.WriteLine(msg);
            //#endif
        }

        // Output warning message if loglevel suitable.
        public static void Warn(string msg)
        {
            if (loglevel < loglevelIndex["WARN"])
                return; 
            
            logger.Warn(msg); 
            msg = GetFormatedTimeNow()+ "|WARN|"+ msg;
            ViewerAddLine(msg);
            
            //if DEBUG
            //    Trace.WriteLine(msg);
            //#endif
        }

        // Output error message without check loglevel.
        public static void Error(string msg)
        {
            logger.Error(msg);

            msg = GetFormatedTimeNow()+ "|ERROR|"+ msg;
            ViewerAddLine(msg);

            //#if DEBUG
            //    Trace.WriteLine(msg);
            //#endif
        }

        
    }
}
