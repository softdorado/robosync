﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows;
using System.Diagnostics;
using System.ComponentModel; // CancelEventArgs

namespace Robosync
{    
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class WindowMain : Window
    {
        // Window to edit properties of task.
        private WindowTask wiTask;

        // Manager object created but never as object used. All functions called as static functions.
        // Create object need to init some elements, e.g config, etc 
        public Manager Mgr;

        public WindowMain()
        {
            InitializeComponent();

            // Create window for task processing (create, change)
            wiTask = new WindowTask();
        }
        
        // On load create manager and window for task processig.
        private void WindowLoad (object sender, RoutedEventArgs e) {

            // Init logviewer.
            Logger.InitLogger();

            // First message bei start application
            Logger.Info("");
            Logger.Info("Start Robosync Version " + Syncer.AppVersion+ " OS "+ Environment.OSVersion);
            Logger.Info("");

            Logger.Info("Working directory "+ Environment.CurrentDirectory);

            // Create manager. By creating read config. Set data-source on greeds.

            Mgr = new Manager(this, wiTask);

            // Make know manager in window-task.
            wiTask.SetManager(Mgr);
        }

        // Start selected tasks or stop if the task is active.
        private void StartTask(object sender, RoutedEventArgs e)
        {
            // Begin stopping only if synchronisation is active and stop-flag is not already active.
            if (Syncer.IsRunning)
            {
                // Begin stopping. Set label in button occure in configManager if stop-exception catched.
                if (!Syncer.IsStopping)
                {
                    Syncer.IsStopping = true;
                    Logger.Info("User has stoped running synchronisation. Waiting for stop.");
                    buStart.Content = "Wait stop";
                }
            }
            else
            {
                // Start synchronisation and switch label in button from "Start" to "Stop"
                buStart.Content = "Stop";

                // Execute synchronisation in separate thread. If it run in the same thread
                // GUI will be frozen. As parameter pass array of selected tasks.
                List<SyncTask> selected = grTasks.SelectedItems.Cast<SyncTask>().ToList();
                Task.Run(() => {
                    Manager.StartTask(selected);
                });
                //Task.Factory.StartNew(() => Manager.StartSync(selected));
            }
        }
        
        // Show  window to create new task.
        private void CreateTask(object sender, RoutedEventArgs e)
        {
            wiTask.Title = "Create new task";
            Manager.NewTask();
        }

        // Delete selected task.
        private void DeleteTask(object sender, RoutedEventArgs e)
        {
            Manager.DeleteTask(grTasks.SelectedIndex);
        }

        // Show logviewer.
        private void ShowLogviewer(object sender, RoutedEventArgs e)
        {
            Logger.ViewerShow();
        }

        // Called by double-click on selected task. Open window to edit/change selected ask.
        private void ChangeTask(object sender, MouseButtonEventArgs e)
        {
            Manager.ChangeTask(grTasks.SelectedIndex);
        }

        // After event "main window closed", exit program, because it happen not automaticaly.
        private void WindowClosed(object sender, EventArgs e)
        {
            Logger.Info("Closed main window");
            Environment.Exit(0);
        }

        private void ShowAbout(object sender, RoutedEventArgs e)
        {
            string about =
               "Robosync Version " + Syncer.AppVersion + "\n" +
               "Copyright (c) 2021, Andrej Koslov\n" +
               "Dustributed under BSD-3 License";
            MessageBox.Show(about, "ABOUT", MessageBoxButton.OK);
        }
        
    }
}
