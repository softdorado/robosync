# Developer guide

## Version

Version X.Y.Z.W

X - Major changes.
Y - New features or functions changed.
Z - Fix error without functional changes, Changed labels on GUI, or text in logfile.
    Changes in documentation.

## Variables Prefix

lb - Label
fr - Frame
re - Regular expression
bt - Button
tx - Text Box
co - Column
gr - Greed
cb - Check-Box
di - DirectoryInfo
ar - Array
rb - Richt-Text-box

## Contributor license

https://opensource.org/license/bsd-3-clause