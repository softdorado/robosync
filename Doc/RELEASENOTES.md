# Version 3.0.0

- Use catalog (catalog.zip in root of destination) to save information about saved data. 
  It extrem speed up detecting changes.

# Version 2.2.0

- Authentification with username and passwort if destination on network share.
- Cange opacity elemente depend on mode (synchronisation or backup)

# Version 2.1.0.0

## Fixed issue
- Fix problem "remove item from source not work"

## New functionality
- In status of task show amount of remaining data in percent.
  Befor "Running ". Now "Running XX%"

# Version 2.0.0.0

Rewrite as WFP application without functional changes.