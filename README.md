# RoboSync

The program with GUI for simple backup or one way synchronization for Windows. 

Update files in destination if size or last write-time changed.
By synchronization skip special windows files and directories like Links, System-Files, 
Offline-Files. Read-Only flag on files in destination will be removed, because this files 
impossible update later.  Copy create-time and last-write-time.

More documenation in directroy .\doc.