﻿using System.Windows;
using System.Windows.Forms;
using System.ComponentModel; // CancelEventArgs

namespace Robosync
{
    /// <summary>
    /// Interaktionslogik für Window1.xaml
    /// </summary>
    public partial class WindowTask : Window
    {
        private Manager Mgr;

        public WindowTask()
        {
            InitializeComponent();
        }

        private void WindowLoad(object sender, RoutedEventArgs e)
        {
        }

        // Use the same manager as main window.
        public void SetManager(Manager M)
        {
            Mgr = M;
        }

        // Open dialog to browse destination for the task and save result in task.
        // In .Net Core this function not exist, so I changed to standard .Net
        private void BrowseDestination(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            DialogResult result = dialog.ShowDialog();
            
            if (result.ToString() == "OK")
            {
                txDest.Text = dialog.SelectedPath;
            }
        }

        // Open dialog to browse synchronisation directroy and save result in task.
        private void BrowseSourceSync(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            DialogResult result = dialog.ShowDialog();

            if (result.ToString() == "OK")
            {
                this.txSource.Text = dialog.SelectedPath;
            }
        }
        
        // Open dialog to browse source directroy for backup and add to ListBox of sourses.
        private void BrowseSourceBackup(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            DialogResult result = dialog.ShowDialog();

            if (result.ToString() == "OK")
            {
                liSource.Items.Add(dialog.SelectedPath);
            }
        }

        // In task select backup mode. I added "if" because at beginn called if Manager
        // not initialised. I think this function called during initialisation to set
        // default state.
        private void rbModeBackupEnable(object sender, RoutedEventArgs e)
        {
            if(Mgr != null) Manager.setBackupMode();
        }

        // In task select synchronisation mode.
        private void rbModeSyncEnable(object sender, RoutedEventArgs e)
        { 
            if (Mgr != null) Manager.setSyncMode();
        }

        // Remove one directroy from list of backups.
        private void SourceBackupRemoveItem(object sender, RoutedEventArgs e)
        {
            Logger.Info("Remove item from source. Index " + liSource.SelectedIndex);
            if (liSource.SelectedIndex != -1 ) {
                liSource.Items.RemoveAt(liSource.SelectedIndex);
            }
        }

        // Save taske and config to file.
        private void TaskSave(object sender, RoutedEventArgs e)
        {
            Logger.Info("Save task");
            Manager.SaveTask();
        }

        // Copy task and save config to file.
        private void TaskCopy(object sender, RoutedEventArgs e)
        {
            Logger.Info("Copy task");
            Manager.CopyTask();
        }

        // Cancel "standard closing" windows. Instead hide the window an cancel processing
        // closing-event. This need because after "standard close" FWP window, second time
        // ShowDialog() throw exception.
        private void CloseWindow(object sender, CancelEventArgs e)
        {
            Hide();
            e.Cancel = true;
        }
    }
}
