﻿## Skrip called after application as release builded. It remove "unused" files
## in release directroy and create ZIP-Package. 

## Path to release directory
$reDir = "bin\Release"

## Path to archive directroy relativeprojekt direcory
$arDir = "..\Releases"

## Config ZIP archive
$package = @{
  Path = "Robosync.exe", "Robosync.exe.manifest", "NLog.dll", "NLog.config"
  DestinationPath  = ""
}

## Add directory with doc
$docDir = ".\doc"

#region Functions

## Make possible error-message as simple red string on console. Default with  write-error it is
## not possible.To output your error on console $Host.UI.WriteErrorLine("Your error message")
Write-Error -Message "" -ErrorAction SilentlyContinue

## Short form to output error message. It works only with priviously adjustment!
function Out-Error ($message) {
   $Host.UI.WriteErrorLine($message)
}

function Out-Debug ($message) {
   $Host.UI.WriteErrorLine($message)
}

#endregion Functions

#region Processing

## First change to start dir.
cd $PSScriptRoot

if(!(Test-Path -Path $reDir)) {
    Out-Error "Release directory $reDir not found! Enter to continue."
    Read-Host
    Exit 1
}

## Check if archive directory exists, create if not, and reassign to variable.
$arDir = New-Item -ItemType Directory -Force -Path $arDir

## Set path to new zip archiv
$package.DestinationPath = $arDir.FullName+ "\Robosync_New.zip"

## Remove old zip
Remove-Item -Path $package.DestinationPath -Force -ErrorAction Ignore

## Change to release dir
Set-Location -Path $reDir

## Create zip
Compress-Archive @package

## Add to ZIP Direcoty with docs
Compress-Archive -Path $docDir -Update -DestinationPath $package.DestinationPath

#endregion Processing