﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Robosync
{
    /// <summary>
    /// Interaktionslogik für WindowLogviewer.xaml
    /// </summary>
    public partial class WindowLogviewer : Window
    {
        public WindowLogviewer()
        {
            InitializeComponent();
        }

        // Load in viewer selected logfile.
        private void OpenLogfile(object sender, RoutedEventArgs e)
        {
            Logger.OpenLogfile();
        }
    }
 }